from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.profile, name='profile'),
	path('about/', views.about, name='about'),
	path('contact/', views.contact, name='contact'),
	path('foto/', views.foto, name='foto'),]